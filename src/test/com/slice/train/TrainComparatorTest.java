package test.com.slice.train;

import main.com.slice.train.models.Train;
import main.com.slice.train.services.TrainComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class TrainComparatorTest {




    static List<Arguments> trainComparatorTestArgs () {
        Train train1 = new Train("Hogwarts", "9and3/4", 16.00, 170);
        Train train2 = new Train("Hogwarts", "2", 9.34, 13);
        Train train3 = new Train("Neverland", "3", 13.13, 170);
        Train train4 = new Train("aVrora", "4A", 3.33, 13);
        Train train5 = new Train("aUrora", "5", 3.33, 170);
        Train train6 = new Train("Neverland", "6", 00.00, 170);
        Train train7 = new Train("Kyiv", "7HOME", 7.00, 170);

        List<Train> trainsToSort = new ArrayList<>(Arrays.asList(train1, train2, train3, train4, train5, train6, train7));
        List<Train> trainsExp = new ArrayList<>(Arrays.asList(train2,train4, train1, train7, train6, train3, train5));


        TrainComparator comparator = new TrainComparator();

        return List.of(
                Arguments.arguments(trainsToSort, trainsExp, comparator)
        );
    }

    @ParameterizedTest
    @MethodSource("trainComparatorTestArgs")
    void trainComparatorTest(List<Train> actual, List<Train> expected, Comparator<Train> comparator){
        actual.sort(comparator);
        Assertions.assertEquals(actual, expected);
    }


//        �������� ����� ��� ���������� Train �� ����� ����,
//        ���� ����� ���� ��������� - ����� ������������� � �� ������
//        ����������, ���� � ����� ���� � ����� ���������� ���������,
//                ����� ���� � ����� ���������� ���������� - ����� �����������.


}
